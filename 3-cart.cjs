const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 

// Q1. Find all the items with price more than $65.

// const itemsWhosPriceMoreThan65=products.map((item)=>{
//     //console.log(item)
//     console.log(typeof(item))
//     if(typeof(item)==Object)
//     {
//         console.log(item)
//     }
    //const objectsInItem=Object.values(item)
    // const itemObject=Object.values(objectsInItem).reduce((product,currPrice)=>{
    //     const productPrice=Number(objectsInItem[currPrice].price.replace('$',''))
    //     console.log(productPrice)
    // })
    //console.log(itemObject);
//})

let productPrice=[];
const itemsWhosPriceMoreThan65=products.map((item)=>{
    Object.values(item).map((product)=>{
        if(typeof product.price==='string' && parseInt(product.price.slice(1))>65)
        {
            productPrice.push(product)
        }
    })
    console.log(productPrice);
})

//Q2. Find all the items where quantity ordered is more than 1.


//Q.3 Get all items which are mentioned as fragile.


//Q.4 Find the least and the most expensive item for a single quantity.


//Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)